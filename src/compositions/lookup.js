import { HEROES, ROLES } from '../constants';

export default function () {
	function findHeroById(id) {
		return Object.values(HEROES).find((hero) => {
      return id == hero.ID
    })
	}

	function findRoleById(id) {
		return Object.values(ROLES).find((role) => {
      return id == role.ID
    })
	}

  return { findHeroById, findRoleById }
}
