export const HERO_DIS = {
  ID: 10,
  NAME: "Dis",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 186,
      TITLE: "",
      LABEL: "Dis 1",
    },
    HERO_SKILL_2: {
      ID: 119,
      TITLE: "",
      LABEL: "Dis 2",
    },
    HERO_SKILL_3: {
      ID: 119,
      TITLE: "",
      LABEL: "Dis 3",
    },
    HERO_SKILL_4: {
      ID: 113,
      TITLE: "",
      LABEL: "Dis 4",
    },
    HERO_SKILL_5: {
      ID: 112,
      TITLE: "",
      LABEL: "Dis 5",
    },
  }
}