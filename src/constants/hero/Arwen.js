export const HERO_ARWEN = {
  ID: 7,
  NAME: "Arwen",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 125,
      TITLE: "",
      LABEL: "Arwen 1",
    },
    HERO_SKILL_2: {
      ID: 125,
      TITLE: "",
      LABEL: "Arwen 2",
    },
    HERO_SKILL_3: {
      ID: 126,
      TITLE: "",
      LABEL: "Arwen 3",
    },
    HERO_SKILL_4: {
      ID: 123,
      TITLE: "",
      LABEL: "Arwen 4",
    },
    HERO_SKILL_5: {
      ID: 97,
      TITLE: "",
      LABEL: "Arwen 5",
    },
  }
}