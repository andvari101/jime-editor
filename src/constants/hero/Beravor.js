export const HERO_BERAVOR = {
  ID: 2,
  NAME: "Beravor",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 95,
      TITLE: "",
      LABEL: "Beravor 1",
    },
    HERO_SKILL_2: {
      ID: 92,
      TITLE: "",
      LABEL: "Beravor 2",
    },
    HERO_SKILL_3: {
      ID: 93,
      TITLE: "",
      LABEL: "Beravor 3",
    },
    HERO_SKILL_4: {
      ID: 93,
      TITLE: "",
      LABEL: "Beravor 4",
    },
    HERO_SKILL_5: {
      ID: 94,
      TITLE: "",
      LABEL: "Beravor 5",
    },
  }
}