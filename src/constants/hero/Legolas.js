export const HERO_LEGOLAS = {
  ID: 6,
  NAME: "Legolas",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 104,
      TITLE: "",
      LABEL: "Legolas 1",
    },
    HERO_SKILL_2: {
      ID: 105,
      TITLE: "",
      LABEL: "Legolas 2",
    },
    HERO_SKILL_3: {
      ID: 105,
      TITLE: "",
      LABEL: "Legolas 3",
    },
    HERO_SKILL_4: {
      ID: 106,
      TITLE: "",
      LABEL: "Legolas 4",
    },
    HERO_SKILL_5: {
      ID: 98,
      TITLE: "",
      LABEL: "Legolas 5",
    },
  }
}