export const HERO_ELEANOR = {
  ID: 9,
  NAME: "Eleanor",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 94,
      TITLE: "",
      LABEL: "Eleanor 1",
    },
    HERO_SKILL_2: {
      ID: 124,
      TITLE: "",
      LABEL: "Eleanor 2",
    },
    HERO_SKILL_3: {
      ID: 124,
      TITLE: "",
      LABEL: "Eleanor 3",
    },
    HERO_SKILL_4: {
      ID: 123,
      TITLE: "",
      LABEL: "Eleanor 4",
    },
    HERO_SKILL_5: {
      ID: 95,
      TITLE: "",
      LABEL: "Eleanor 5",
    },
  }
}