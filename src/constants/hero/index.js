import { HERO_ARAGORN } from './Aragorn.js'
import { HERO_BERAVOR } from './Beravor.js'
import { HERO_BILBO } from './Bilbo.js'
import { HERO_ELENA } from './Elena.js'
import { HERO_GIMLI } from './Gimli.js'
import { HERO_LEGOLAS } from './Legolas.js'
import { HERO_ARWEN } from './Arwen.js'
import { HERO_GANDALF } from './Gandalf.js'
import { HERO_ELEANOR } from './Eleanor.js'
import { HERO_DIS } from './Dis.js'
import { HERO_BALIN } from './Balin.js'

export const HEROES = {
  HERO_ARAGORN,
  HERO_BERAVOR,
  HERO_BILBO,
  HERO_ELENA,
  HERO_GIMLI,
  HERO_LEGOLAS,
  HERO_ARWEN,
  HERO_GANDALF,
  HERO_ELEANOR,
  HERO_DIS,
  HERO_BALIN,
}