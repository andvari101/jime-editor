export const HERO_GIMLI = {
  ID: 5,
  NAME: "Gimli",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 110,
      TITLE: "",
      LABEL: "Gimli 1",
    },
    HERO_SKILL_2: {
      ID: 111,
      TITLE: "",
      LABEL: "Gimli 2",
    },
    HERO_SKILL_3: {
      ID: 111,
      TITLE: "",
      LABEL: "Gimli 3",
    },
    HERO_SKILL_4: {
      ID: 113,
      TITLE: "",
      LABEL: "Gimli 4",
    },
    HERO_SKILL_5: {
      ID: 112,
      TITLE: "",
      LABEL: "Gimli 5",
    },
  }
}