export const HERO_BALIN = {
  ID: 11,
  NAME: "Balin",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 112,
      TITLE: "",
      LABEL: "Balin 1",
    },
    HERO_SKILL_2: {
      ID: 121,
      TITLE: "",
      LABEL: "Balin 2",
    },
    HERO_SKILL_3: {
      ID: 121,
      TITLE: "",
      LABEL: "Balin 3",
    },
    HERO_SKILL_4: {
      ID: 120,
      TITLE: "",
      LABEL: "Balin 4",
    },
    HERO_SKILL_5: {
      ID: 122,
      TITLE: "",
      LABEL: "Balin 5",
    },
  }
}