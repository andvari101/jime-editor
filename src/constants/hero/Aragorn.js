export const HERO_ARAGORN = {
  ID: 1,
  NAME: "Aragorn",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 107,
      TITLE: "",
      LABEL: "Aragorn 1",
    },
    HERO_SKILL_2: {
      ID: 108,
      TITLE: "",
      LABEL: "Aragorn 2",
    },
    HERO_SKILL_3: {
      ID: 108,
      TITLE: "",
      LABEL: "Aragorn 3",
    },
    HERO_SKILL_4: {
      ID: 109,
      TITLE: "",
      LABEL: "Aragorn 4",
    },
    HERO_SKILL_5: {
      ID: 95,
      TITLE: "",
      LABEL: "Aragorn 5",
    },
  }
}