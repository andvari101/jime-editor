export const HERO_ELENA = {
  ID: 4,
  NAME: "Elena",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 96,
      TITLE: "",
      LABEL: "Elena 1",
    },
    HERO_SKILL_2: {
      ID: 98,
      TITLE: "",
      LABEL: "Elena 2",
    },
    HERO_SKILL_3: {
      ID: 99,
      TITLE: "",
      LABEL: "Elena 3",
    },
    HERO_SKILL_4: {
      ID: 99,
      TITLE: "",
      LABEL: "Elena 4",
    },
    HERO_SKILL_5: {
      ID: 97,
      TITLE: "",
      LABEL: "Elena 5",
    },
  }
}