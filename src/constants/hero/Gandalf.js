export const HERO_GANDALF = {
  ID: 8,
  NAME: "Gandalf",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 118,
      TITLE: "Grey Pilgrim",
      LABEL: "Gandalf 1",
    },
    HERO_SKILL_2: {
      ID: 127,
      TITLE: "Uncanny",
      LABEL: "Gandalf 2",
    },
    HERO_SKILL_3: {
      ID: 128,
      TITLE: "Flame Imperishable",
      LABEL: "Gandalf 3",
    },
    HERO_SKILL_4: {
      ID: 129,
      TITLE: "Greater Power",
      LABEL: "Gandalf 4",
    },
    HERO_SKILL_5: {
      ID: 130,
      TITLE: "Dazzling Flash",
      LABEL: "Gandalf 5",
    },
  }
}