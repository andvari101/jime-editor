export const HERO_BILBO = {
  ID: 3,
  NAME: "Bilbo",
  HERO_SKILLS: {
    HERO_SKILL_1: {
      ID: 100,
      TITLE: "",
      LABEL: "Bilbo 1",
    },
    HERO_SKILL_2: {
      ID: 100,
      TITLE: "",
      LABEL: "Bilbo 2",
    },
    HERO_SKILL_3: {
      ID: 102,
      TITLE: "",
      LABEL: "Bilbo 3",
    },
    HERO_SKILL_4: {
      ID: 103,
      TITLE: "",
      LABEL: "Bilbo 4",
    },
    HERO_SKILL_5: {
      ID: 101,
      TITLE: "",
      LABEL: "Bilbo 5",
    },
  }
}