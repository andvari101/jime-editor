export const ROLE_HERBALIST = {
  ID: 7,
  NAME: "Herbalist",
  ROLE_SKILLS: {
    ROLE_SKILL_1: {
      ID: 110,
      TITLE: "",
      LABEL: "Herbalist 1",
    },
    ROLE_SKILL_2: {
      ID: 111,
      TITLE: "",
      LABEL: "Herbalist 2",
    },
    ROLE_SKILL_3: {
      ID: 111,
      TITLE: "",
      LABEL: "Herbalist 3",
    },
    ROLE_SKILL_4: {
      ID: 113,
      TITLE: "",
      LABEL: "Herbalist 4",
    },
    ROLE_SKILL_5: {
      ID: 112,
      TITLE: "",
      LABEL: "Herbalist 5",
    },
    ROLE_SKILL_6: {
      ID: 110,
      TITLE: "",
      LABEL: "Herbalist 6",
    },
    ROLE_SKILL_7: {
      ID: 111,
      TITLE: "",
      LABEL: "Herbalist 7",
    },
    ROLE_SKILL_8: {
      ID: 111,
      TITLE: "",
      LABEL: "Herbalist 8",
    },
    ROLE_SKILL_9: {
      ID: 113,
      TITLE: "",
      LABEL: "Herbalist 9",
    },
    ROLE_SKILL_10: {
      ID: 112,
      TITLE: "",
      LABEL: "Herbalist 10",
    },
    ROLE_SKILL_11: {
      ID: 113,
      TITLE: "",
      LABEL: "Herbalist 11",
    },
    ROLE_SKILL_12: {
      ID: 112,
      TITLE: "",
      LABEL: "Herbalist 12",
    },
  }
}