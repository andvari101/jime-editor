export const ROLE_MEDDLER = {
  ID: 11,
  NAME: "Meddler",
  ROLE_SKILLS: {
    ROLE_SKILL_1: {
      ID: 110,
      TITLE: "",
      LABEL: "Meddler 1",
    },
    ROLE_SKILL_2: {
      ID: 111,
      TITLE: "",
      LABEL: "Meddler 2",
    },
    ROLE_SKILL_3: {
      ID: 111,
      TITLE: "",
      LABEL: "Meddler 3",
    },
    ROLE_SKILL_4: {
      ID: 113,
      TITLE: "",
      LABEL: "Meddler 4",
    },
    ROLE_SKILL_5: {
      ID: 112,
      TITLE: "",
      LABEL: "Meddler 5",
    },
    ROLE_SKILL_6: {
      ID: 110,
      TITLE: "",
      LABEL: "Meddler 6",
    },
    ROLE_SKILL_7: {
      ID: 111,
      TITLE: "",
      LABEL: "Meddler 7",
    },
    ROLE_SKILL_8: {
      ID: 111,
      TITLE: "",
      LABEL: "Meddler 8",
    },
    ROLE_SKILL_9: {
      ID: 113,
      TITLE: "",
      LABEL: "Meddler 9",
    },
    ROLE_SKILL_10: {
      ID: 112,
      TITLE: "",
      LABEL: "Meddler 10",
    },
    ROLE_SKILL_11: {
      ID: 113,
      TITLE: "",
      LABEL: "Meddler 11",
    },
    ROLE_SKILL_12: {
      ID: 112,
      TITLE: "",
      LABEL: "Meddler 12",
    },
  }
}