export const ROLE_MUSICIAN = {
  ID: 3,
  NAME: "Musician",
  ROLE_SKILLS: {
    ROLE_SKILL_1: {
      ID: 110,
      TITLE: "",
      LABEL: "Musician 1",
    },
    ROLE_SKILL_2: {
      ID: 111,
      TITLE: "",
      LABEL: "Musician 2",
    },
    ROLE_SKILL_3: {
      ID: 111,
      TITLE: "",
      LABEL: "Musician 3",
    },
    ROLE_SKILL_4: {
      ID: 113,
      TITLE: "",
      LABEL: "Musician 4",
    },
    ROLE_SKILL_5: {
      ID: 112,
      TITLE: "",
      LABEL: "Musician 5",
    },
    ROLE_SKILL_6: {
      ID: 110,
      TITLE: "",
      LABEL: "Musician 6",
    },
    ROLE_SKILL_7: {
      ID: 111,
      TITLE: "",
      LABEL: "Musician 7",
    },
    ROLE_SKILL_8: {
      ID: 111,
      TITLE: "",
      LABEL: "Musician 8",
    },
    ROLE_SKILL_9: {
      ID: 113,
      TITLE: "",
      LABEL: "Musician 9",
    },
    ROLE_SKILL_10: {
      ID: 112,
      TITLE: "",
      LABEL: "Musician 10",
    },
    ROLE_SKILL_11: {
      ID: 113,
      TITLE: "",
      LABEL: "Musician 11",
    },
    ROLE_SKILL_12: {
      ID: 112,
      TITLE: "",
      LABEL: "Musician 12",
    },
  }
}