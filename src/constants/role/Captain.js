export const ROLE_CAPTAIN = {
  ID: 4,
  NAME: "Captain",
  ROLE_SKILLS: {
    ROLE_SKILL_1: {
      ID: 110,
      TITLE: "",
      LABEL: "Captain 1",
    },
    ROLE_SKILL_2: {
      ID: 111,
      TITLE: "",
      LABEL: "Captain 2",
    },
    ROLE_SKILL_3: {
      ID: 111,
      TITLE: "",
      LABEL: "Captain 3",
    },
    ROLE_SKILL_4: {
      ID: 113,
      TITLE: "",
      LABEL: "Captain 4",
    },
    ROLE_SKILL_5: {
      ID: 112,
      TITLE: "",
      LABEL: "Captain 5",
    },
    ROLE_SKILL_6: {
      ID: 110,
      TITLE: "",
      LABEL: "Captain 6",
    },
    ROLE_SKILL_7: {
      ID: 111,
      TITLE: "",
      LABEL: "Captain 7",
    },
    ROLE_SKILL_8: {
      ID: 111,
      TITLE: "",
      LABEL: "Captain 8",
    },
    ROLE_SKILL_9: {
      ID: 113,
      TITLE: "",
      LABEL: "Captain 9",
    },
    ROLE_SKILL_10: {
      ID: 112,
      TITLE: "",
      LABEL: "Captain 10",
    },
    ROLE_SKILL_11: {
      ID: 113,
      TITLE: "",
      LABEL: "Captain 11",
    },
    ROLE_SKILL_12: {
      ID: 112,
      TITLE: "",
      LABEL: "Captain 12",
    },
  }
}