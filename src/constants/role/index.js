import { ROLE_BURGLAR } from './Burglar.js'
import { ROLE_GUARDIAN } from './Guardian.js'
import { ROLE_MUSICIAN } from './Musician'
import { ROLE_CAPTAIN } from './Captain'
import { ROLE_PATHFINDER } from './Pathfinder'
import { ROLE_HUNTER } from './Hunter'
import { ROLE_HERBALIST } from './Herbalist'
import { ROLE_DELVER } from './Delver'
import { ROLE_SMITH } from './Smith'
import { ROLE_TRAVELER } from './Traveler'
import { ROLE_MEDDLER } from './Meddler'

export const ROLES = {
  ROLE_BURGLAR,
  ROLE_GUARDIAN,
  ROLE_MUSICIAN,
  ROLE_CAPTAIN,
  ROLE_PATHFINDER,
  ROLE_HUNTER,
  ROLE_HERBALIST,
  ROLE_DELVER,
  ROLE_SMITH,
  ROLE_TRAVELER,
  ROLE_MEDDLER,
}