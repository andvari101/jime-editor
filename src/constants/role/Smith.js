export const ROLE_SMITH = {
  ID: 9,
  NAME: "Smith",
  ROLE_SKILLS: {
    ROLE_SKILL_1: {
      ID: 110,
      TITLE: "",
      LABEL: "Smith 1",
    },
    ROLE_SKILL_2: {
      ID: 111,
      TITLE: "",
      LABEL: "Smith 2",
    },
    ROLE_SKILL_3: {
      ID: 111,
      TITLE: "",
      LABEL: "Smith 3",
    },
    ROLE_SKILL_4: {
      ID: 113,
      TITLE: "",
      LABEL: "Smith 4",
    },
    ROLE_SKILL_5: {
      ID: 112,
      TITLE: "",
      LABEL: "Smith 5",
    },
    ROLE_SKILL_6: {
      ID: 110,
      TITLE: "",
      LABEL: "Smith 6",
    },
    ROLE_SKILL_7: {
      ID: 111,
      TITLE: "",
      LABEL: "Smith 7",
    },
    ROLE_SKILL_8: {
      ID: 111,
      TITLE: "",
      LABEL: "Smith 8",
    },
    ROLE_SKILL_9: {
      ID: 113,
      TITLE: "",
      LABEL: "Smith 9",
    },
    ROLE_SKILL_10: {
      ID: 112,
      TITLE: "",
      LABEL: "Smith 10",
    },
    ROLE_SKILL_11: {
      ID: 113,
      TITLE: "",
      LABEL: "Smith 11",
    },
    ROLE_SKILL_12: {
      ID: 112,
      TITLE: "",
      LABEL: "Smith 12",
    },
  }
}